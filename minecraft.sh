if [ ! -f server.jar ]; then

if [ $1 == "1.7.10" ]
then
    echo -e "Downloading MC 1.7.10"
    echo -e "==============================="
  curl -o server.jar https://cdn.nyasky.cf/paper/PaperSpigot-1.7.10.jar >> output.txt
elif [ $1 == "1.17" ]
then
    echo -e "Downloading MC 1.17"
    echo -e "==============================="
  curl -o server.jar https://gitlab.com/pterodactyl-fixes/jarfiles/-/raw/main/spigot-1.17.jar
else
    echo -e "Downloading MC ${1}"
    echo -e "==============================="
  curl -o server.jar https://papermc.io/api/v1/paper/${1}/latest/download  >> output.txt
fi

fi
  java ${JAVA_ARGUMENT} -Xms256M -Xmx${2}M -Dterminal.jline=false -Dfile.encoding=UTF-8 -Dterminal.ansi=true -jar server.jar

