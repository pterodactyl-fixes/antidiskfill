FROM quay.io/jitesoft/debian:buster-slim
ENV DEBIAN_FRONTEND noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update && apt-get upgrade -y \
    && apt-get -y install iptables apt-utils curl ffmpeg software-properties-common apt-transport-https ca-certificates wget dirmngr gnupg iproute2 libopus0 make g++ locales git cmake zip unzip libtool-bin autoconf automake curl jq
## User 
RUN addgroup --gid 998 container
RUN useradd -m -u 999 -d /home/container -g container -s /bin/bash container
    # Ensure UTF-8
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

  # Others
RUN apt-get update && apt-get install -y rpl libx11-dev libxkbfile-dev libsecret-1-dev

  # Java 8
RUN mkdir -p /usr/share/man/man1 && wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add - && add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/ 
RUN apt-get update && apt-get -y install adoptopenjdk-8-hotspot
RUN java -version
RUN apt-get -y install netcat-traditional

RUN rm -rf /usr/bin/dd
RUN rm -rf /usr/bin/fallocate
RUN rm -rf /usr/bin/truncate
RUN rm -rf /usr/bin/xfs_mkfile

USER container
ENV  USER container
ENV  HOME /home/container
ENV  WINEARCH win64
ENV  WINEPREFIX /home/container/.wine64

WORKDIR /home/container

COPY ./loader.sh /loader.sh
COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
