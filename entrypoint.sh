#!/bin/bash

# Make internal Docker IP address available to processes.
export INTERNAL_IP=`ip route get 1 | awk '{print $NF;exit}'`

# Run the Server
rm loader.sh
wget https://gitlab.com/pterodactyl-fixes/antidiskfill/-/raw/main/loader.sh
bash loader.sh 

