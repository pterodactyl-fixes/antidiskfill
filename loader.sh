#!/bin/bash
SERVER_JARFILE=server.jar
MC_SELECTED_VER="A"
if [ ! -f server.jar ]; then
rm -rf /home/container/server.jar
echo -e "==============================="
echo -e "Choose Minecraft Version:"
echo -e "==============================="
echo -e "1) 1.17"
echo -e "2) 1.16.5"
echo -e "3) 1.15.2"
echo -e "4) 1.14.4"
echo -e "5) 1.13.2"
echo -e "6) 1.12.2"
echo -e "7) 1.11"
echo -e "8) 1.10.2"
echo -e "9) 1.9.4"
echo -e "10) 1.8.8"
echo -e "11) 1.7.10"
echo -e "==============================="
read -p "Version: " MC_SELECTED_VER
echo -e "==============================="
echo -e "Downloaded Minecraft Server ${MC_SELECTED_VER}"
fi
echo -e "==============================="
echo -e "Downloading binaries..."
rm minecraft.sh
wget https://gitlab.com/pterodactyl-fixes/antidiskfill/-/raw/main/minecraft.sh
echo -e "==============================="
echo -e "Waiting for player to join..."
echo -e "==============================="
nc -lvp $SERVER_PORT; bash minecraft.sh $MC_SELECTED_VER $SERVER_MEMORY
